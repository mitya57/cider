## CIDER's nREPL middleware

Much of CIDER's functionality depends on the presence of CIDER's own
[nREPL
middleware](https://github.com/clojure-emacs/cider-nrepl). Starting
with version 0.11, When `cider-jack-in` (<kbd>C-c C-x (C-)j (C-)j</kbd>) is used,
CIDER takes care of injecting it and its other dependencies.

**`profiles.clj` or `profile.boot` don't need to be modified anymore for the above use case!**

If you don't want `cider-jack-in` to inject dependencies automatically, set
`cider-inject-dependencies-at-jack-in` to `nil`. Note that you'll have to setup
the dependencies yourself (see the section below), just as in CIDER 0.10 and older.

CIDER can also inject a Clojure dependency into your project, which is useful,
for example, if your project defaults to an older version of Clojure than that
supported by the CIDER middleware. Set `cider-jack-in-auto-inject-clojure`
appropriately to enable this.

If a standalone REPL is preferred, you need to invoke `cider-connect` (instead
of `cider-jack-in`) and you'll need to manually add the dependencies to your
Clojure project (explained in the following section).

### Setting up a standalone REPL

#### Using Leiningen

Use the convenient plugin for defaults, either in your project's
`project.clj` file or in the :repl profile in `~/.lein/profiles.clj`.

```clojure
:plugins [[cider/cider-nrepl "x.y.z"]]
```

A minimal `profiles.clj` for CIDER would be:

```clojure
{:repl {:plugins [[cider/cider-nrepl "0.18.0"]]}}
```

!!! warning

    Be careful not to place this in the `:user` profile, as this way CIDER's
    middleware will always get loaded, causing `lein` to start slower.  You really
    need it just for `lein repl` and this is what the `:repl` profile is for.

#### Using Boot

Boot users can configure the tool to include the middleware automatically in
all of their projects using a `~/.boot/profile.boot` file like so:

```clojure
(require 'boot.repl)

(swap! boot.repl/*default-dependencies*
       concat '[[cider/cider-nrepl "0.18.0"]])

(swap! boot.repl/*default-middleware*
       conj 'cider.nrepl/cider-middleware)
```

For more information visit [boot-clj wiki](https://github.com/boot-clj/boot/wiki/Cider-REPL).

### Using embedded nREPL server

If you're embedding nREPL in your application you'll have to start the
server with CIDER's own nREPL handler.

```clojure
(ns my-app
  (:require [nrepl.server :as nrepl-server]
            [cider.nrepl :refer (cider-nrepl-handler)]))

(defn -main
  []
  (nrepl-server/start-server :port 7888 :handler cider-nrepl-handler))
```

It goes without saying that your project should depend on `cider-nrepl`.

!!! note

    `x.y.z` should be compatible with the version of CIDER you're currently using.
    The required version can be checked in `cider-required-middleware-version`.
    Prior to CIDER 0.18, CIDER and cider-nrepl were always released together
    and their versions had to match for things to work, but as the prominence
    of cider-nrepl grew and many other tools started using, this was changed.
